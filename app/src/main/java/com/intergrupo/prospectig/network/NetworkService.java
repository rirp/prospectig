package com.intergrupo.prospectig.network;

import com.intergrupo.prospectig.entities.Prospect;
import com.intergrupo.prospectig.entities.SessionData;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**
 * Network service contract
 * @author ronaldruiz on 5/14/18.
 */

public interface NetworkService {
    @GET("/application/login")
    Observable<SessionData> login(@Query("email") String userData, @Query("password") String password);
    @GET("/sch/prospects.json")
    Observable<List<Prospect>> getProspect(@Header("token") String token);
}
