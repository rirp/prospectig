package com.intergrupo.prospectig.helpers;

/**
 * Created by ronaldruiz on 6/26/18.
 */

public class StringHelper {
    private static final int INDEX_DOMAIN               = 1;
    private static final int MAX_VALUE_EMAIL_SEPARATED  = 2;
    private static final String DOMAIN                  = ".com";

    public static boolean validateEmail(String email) {
        String[] splitEmail = email.split("@");

        if (splitEmail.length > 0 && splitEmail.length >= MAX_VALUE_EMAIL_SEPARATED) {
            int length = splitEmail[INDEX_DOMAIN].length();
            if (length > DOMAIN.length()) {
                String domain = splitEmail[INDEX_DOMAIN].substring(length - DOMAIN.length());
                if (domain.equals(DOMAIN)) {
                    return true;
                }
            }
        }
        return false;
    }
}
