package com.intergrupo.prospectig.entities;

/**
 * Created by ronaldruiz on 6/26/18.
 */

public class Credentials {
    private String email;
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
