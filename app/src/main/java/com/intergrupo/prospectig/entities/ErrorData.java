package com.intergrupo.prospectig.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ronaldruiz on 6/26/18.
 */

public class ErrorData {
    @SerializedName("code")
    private Integer code;

    @SerializedName("error")
    private String error;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
