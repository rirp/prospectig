package com.intergrupo.prospectig.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ronaldruiz on 6/26/18.
 */

public class SessionData {
    @SerializedName("success")
    private boolean success;
    @SerializedName("authToken")
    private String authToken;
    @SerializedName("emailEditText")
    private String email;
    @SerializedName("zone")
    private Object zone;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getZone() {
        return zone;
    }

    public void setZone(Object zone) {
        this.zone = zone;
    }

    public void setData(SessionData sessionData) {
        this.success = sessionData.isSuccess();
        this.authToken = sessionData.getAuthToken();
        this.email = sessionData.getEmail();
        this.zone = sessionData.getZone();
    }

    public void clearSession() {
        this.success = false;
        this.authToken = "";
        this.email = "";
        this.zone = "";
    }
}
