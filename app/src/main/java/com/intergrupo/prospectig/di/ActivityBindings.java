package com.intergrupo.prospectig.di;

import com.intergrupo.prospectig.di.scopes.PerActivity;
import com.intergrupo.prospectig.ui.login.LoginActivityModule;
import com.intergrupo.prospectig.ui.login.view.LoginActivity;
import com.intergrupo.prospectig.ui.menu.MainActivity;
import com.intergrupo.prospectig.ui.menu.MainActivityModule;

import dagger.Module;
import dagger.android.AndroidInjectionModule;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by ronaldruiz on 6/25/18.
 */

@Module(includes = AndroidInjectionModule.class)
public interface ActivityBindings {
    @PerActivity
    @ContributesAndroidInjector(modules = {LoginActivityModule.class})
    LoginActivity loginActivityInjector();

    @PerActivity
    @ContributesAndroidInjector(modules = MainActivityModule.class)
    MainActivity mainActivityInjector();
}
