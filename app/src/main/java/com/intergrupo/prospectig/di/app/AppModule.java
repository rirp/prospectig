package com.intergrupo.prospectig.di.app;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.intergrupo.prospectig.R;
import com.intergrupo.prospectig.entities.SessionData;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.android.AndroidInjectionModule;

/**
 * Created by ronaldruiz on 6/25/18.
 */
@Module(includes = AndroidInjectionModule.class)
class AppModule {
    @Provides
    @Singleton
    protected static SessionData sessionDataProvide() {
        return new SessionData();
    }

    @Provides
    @Singleton
    protected static SharedPreferences sharedPreferencesProvide(Application app) {
        Context context = app.getApplicationContext();
        String appName = context.getString(R.string.app_name);
        return app.getSharedPreferences(appName, Context.MODE_PRIVATE);
    }
}
