package com.intergrupo.prospectig.ui.login.presenter;

import com.intergrupo.prospectig.entities.Credentials;
import com.intergrupo.prospectig.ui.login.model.LoginModel;
import com.intergrupo.prospectig.ui.login.model.LoginModelCallback;
import com.intergrupo.prospectig.ui.login.view.LoginView;

import javax.inject.Inject;

/**
 * Created by ronaldruiz on 6/25/18.
 */

public class LoginPresenterImp implements LoginPresenter, LoginModelCallback {
    private static final int UNREGISTER_EMAIL   = 133;
    private static final int WRONG_PASSWORD     = 134;
    private LoginView mView;
    private LoginModel mModel;

    @Inject
    public LoginPresenterImp(LoginView pView, LoginModel pModel) {
        this.mView = pView;
        this.mModel = pModel;
    }

    @Override
    public void login(String pEmail, String pPassword) {
        Credentials data = new Credentials();
        data.setEmail(pEmail);
        data.setPassword(pPassword);
        mModel.login(data, this);
    }

    @Override
    public void onLoginSuccess(boolean isLocalAuthentication) {
        mView.onLoginSuccess();
    }

    @Override
    public void onLoginFailure(int code, String message) {
        switch (code) {
            case UNREGISTER_EMAIL:
                mView.showErrorEmail(message);
                break;
            case WRONG_PASSWORD:
                mView.showErrorPassword(message);
                break;
            default:
                mView.onLoginFailure(message);
                break;
        }
    }
}
