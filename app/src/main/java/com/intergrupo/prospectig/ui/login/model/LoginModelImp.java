package com.intergrupo.prospectig.ui.login.model;

import android.content.SharedPreferences;

import com.intergrupo.prospectig.entities.Credentials;
import com.intergrupo.prospectig.entities.ErrorData;
import com.intergrupo.prospectig.entities.SessionData;
import com.intergrupo.prospectig.network.NetworkService;
import com.intergrupo.prospectig.ui.base.BaseModel;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

import static com.intergrupo.prospectig.helpers.SecurityHelper.decrypt;
import static com.intergrupo.prospectig.helpers.SecurityHelper.encrypt;

/**
 * Created by ronaldruiz on 6/26/18.
 */

public class LoginModelImp extends BaseModel<SessionData, Credentials> implements LoginModel {
    private static final String EMAIL_KEY = "email";
    private static final String PASS_KEY = "pass";

    private LoginModelCallback mCallback;
    private SessionData mSessionData;
    private Retrofit mRetrofit;
    private SharedPreferences mSharedPreferences;
    private Credentials mCredentials;

    public LoginModelImp(Retrofit pRetrofit, SessionData pSessionData, SharedPreferences pPreferences) {
        mRetrofit = pRetrofit;
        mSessionData = pSessionData;
        mSharedPreferences = pPreferences;
    }

    @Override
    public void login(Credentials pCredentials, LoginModelCallback pCallback) {
        if (hasCredentialsSaved()) {
            boolean isSigned = localLogin(pCredentials.getEmail(), pCredentials.getPassword());
            if (isSigned) {
                pCallback.onLoginSuccess(true);
            }
        } else {
            mCallback = pCallback;
            mCredentials = pCredentials;
            getObservable(pCredentials).subscribe(getObserver());
        }
    }

    @Override
    protected DisposableObserver<SessionData> getObserver() {
        return new DisposableObserver<SessionData>() {
            @Override
            public void onNext(SessionData sessionData) {
                if (sessionData.isSuccess()) {
                    mSessionData.setData(sessionData);
                    saveLoginData();
                    mCallback.onLoginSuccess(false);
                }
            }

            @Override
            public void onError(Throwable e) {
                ErrorData errorData = validateError(e);
                mCallback.onLoginFailure(errorData.getCode(), errorData.getError());
            }

            @Override
            public void onComplete() {

            }
        };
    }

    @Override
    protected Observable<SessionData> getObservable(Credentials pData) {
        return mRetrofit.create(NetworkService.class)
                .login(pData.getEmail(), pData.getPassword())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public void saveLoginData() {
        String emailEncrypted = encrypt(mCredentials.getEmail());
        String passwordEncrypted = encrypt(mCredentials.getPassword());
        if (emailEncrypted != null && passwordEncrypted != null) {
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            editor.putString(EMAIL_KEY, emailEncrypted);
            editor.putString(PASS_KEY, passwordEncrypted);
            editor.apply();
        }
    }

    public Credentials getCredentialsFromPreferences() {
        String emailFromPreferences = mSharedPreferences.getString(EMAIL_KEY, null);
        String passwordFromPreferences = mSharedPreferences.getString(PASS_KEY, null);
        if (emailFromPreferences != null && passwordFromPreferences != null) {
            String email = decrypt(emailFromPreferences);
            String password = decrypt(passwordFromPreferences);
            if (email != null && password != null) {
                Credentials credentials = new Credentials();
                credentials.setEmail(email);
                credentials.setPassword(password);
            }
        }
        return null;
    }

    public boolean hasCredentialsSaved() {
        return getCredentialsFromPreferences() != null;
    }

    public boolean localLogin(String email, String password) {
        Credentials credentials = getCredentialsFromPreferences();
        return email.equals(credentials.getEmail()) && password.equals(credentials.getPassword());
    }
}
