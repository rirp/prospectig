package com.intergrupo.prospectig.ui.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.WindowManager;

import com.intergrupo.prospectig.R;

import dagger.android.support.DaggerAppCompatActivity;

/**
 * Created by ronaldruiz on 6/25/18.
 */

public abstract class BaseActivity extends DaggerAppCompatActivity {
    private ProgressDialog mLoadingDialog;
    private static final long TIME_OUT = 3000;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        hideStatusBar();
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        addListenerView();
    }

    private void hideStatusBar() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public abstract void addListenerView();

    private Runnable loadingTimeoutRunnable = new Runnable() {
        @Override
        public void run() {
            removeLoading();
        }
    };

    public void showLoading() {
        if (this.mLoadingDialog == null || !this.mLoadingDialog.isShowing()) {
            this.mLoadingDialog = ProgressDialog
                    .show(this,
                            "",
                            this.getString(R.string.string_loading),
                            true,
                            false,
                            null);
            //timeoutHandler.postDelayed(loadingTimeoutRunnable,  TIME_OUT);
        }
    }

    public void removeLoading() {
        try {
            if (this.mLoadingDialog != null && this.mLoadingDialog.isShowing()) {
                this.mLoadingDialog.dismiss();
                this.mLoadingDialog = null;
            }
        } catch (final IllegalArgumentException e) {
            this.mLoadingDialog = null;
        }
    }
}
