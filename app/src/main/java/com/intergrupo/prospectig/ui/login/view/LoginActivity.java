package com.intergrupo.prospectig.ui.login.view;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.intergrupo.prospectig.R;
import com.intergrupo.prospectig.ui.base.BaseActivity;
import com.intergrupo.prospectig.ui.login.presenter.LoginPresenter;
import com.intergrupo.prospectig.helpers.StringHelper;
import com.intergrupo.prospectig.ui.menu.MainActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A login screen that offers login via emailEditText/passwordEditText.
 */
public class LoginActivity extends BaseActivity implements LoginView, TextWatcher {
    private static final int PERMISSIONS_REQUEST_TELEPHONY = 0;

    @BindView(R.id.email)
    protected EditText emailEditText;
    @BindView(R.id.password)
    protected EditText passwordEditText;
    @BindView(R.id.email_layout)
    TextInputLayout emailLayout;
    @BindView(R.id.password_layout)
    TextInputLayout passwordLayout;
    @BindView(R.id.sign_in_button)
    protected Button signInButton;
    @Inject
    protected LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        requestPermission();
    }

    @Override
    public void addListenerView() {
        emailLayout.setErrorEnabled(true);
        emailEditText.addTextChangedListener(this);
        passwordLayout.setErrorEnabled(true);
        passwordEditText.addTextChangedListener(this);
    }

    @Override
    public void onLoginSuccess() {
        removeLoading();
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public void onLoginFailure(String message) {
        removeLoading();
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showErrorPassword(String message) {
        removeLoading();
        passwordLayout.setError(message);
    }

    @Override
    public void showErrorEmail(String message) {
        removeLoading();
        emailLayout.setError(message);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        // No need implement
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        boolean isEmailValid = StringHelper.validateEmail(emailEditText.getText().toString());
        int length = passwordEditText.getText().toString().length();
        signInButton.setEnabled(isEmailValid && length > 0);
    }

    @Override
    public void afterTextChanged(Editable editable) {
        // No need implement
    }

    @OnClick(R.id.sign_in_button)
    public void onViewClicked() {
        clearErrors();
        showLoading();
        String email = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        presenter.login(email, password);
    }

    private void requestPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CONTACTS)) {

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        PERMISSIONS_REQUEST_TELEPHONY);
            }
        }
    }

    private void clearErrors() {
        emailLayout.setError(null);
        passwordLayout.setError(null);
    }
}

