package com.intergrupo.prospectig.ui.login.presenter;

/**
 * Created by ronaldruiz on 6/25/18.
 */

public interface LoginPresenter {
    void login(String pEmail, String pPassword);
}
