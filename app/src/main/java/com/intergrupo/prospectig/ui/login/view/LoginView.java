package com.intergrupo.prospectig.ui.login.view;

import io.reactivex.annotations.NonNull;

/**
 * Created by ronaldruiz on 6/25/18.
 */

public interface LoginView {
    void onLoginSuccess();
    void onLoginFailure(@NonNull String message);
    void showErrorPassword(@NonNull String message);
    void showErrorEmail(@NonNull String message);
}
