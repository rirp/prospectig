package com.intergrupo.prospectig.ui.login;

import android.content.SharedPreferences;

import com.intergrupo.prospectig.di.scopes.PerActivity;
import com.intergrupo.prospectig.entities.SessionData;
import com.intergrupo.prospectig.ui.login.model.LoginModel;
import com.intergrupo.prospectig.ui.login.model.LoginModelCallback;
import com.intergrupo.prospectig.ui.login.model.LoginModelImp;
import com.intergrupo.prospectig.ui.login.presenter.LoginPresenter;
import com.intergrupo.prospectig.ui.login.presenter.LoginPresenterImp;
import com.intergrupo.prospectig.ui.login.view.LoginActivity;
import com.intergrupo.prospectig.ui.login.view.LoginView;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by ronaldruiz on 6/25/18.
 */

@Module
public abstract class LoginActivityModule {
    @Binds
    @PerActivity
    abstract LoginView loginViewInjector(LoginActivity loginActivity);

    @Binds
    @PerActivity
    abstract LoginPresenter loginPresenterInjector(LoginPresenterImp presenter);

   @Binds
   @PerActivity
   abstract LoginModelCallback loginModelCallbackInjector(LoginPresenterImp presenter);

    @Provides
    @PerActivity
    protected static LoginModel loginModelInjector(Retrofit retrofit, SessionData sessionData, SharedPreferences pPreferences) {
        return new LoginModelImp(retrofit, sessionData, pPreferences);
    }
}
