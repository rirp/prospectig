package com.intergrupo.prospectig.ui.login.model;

import com.intergrupo.prospectig.entities.Credentials;

/**
 * Created by ronaldruiz on 6/26/18.
 */

public interface LoginModel {
    void login(Credentials pData, LoginModelCallback mCallback);
}
