package com.intergrupo.prospectig.ui.base;

import android.util.Log;

import com.google.gson.Gson;
import com.intergrupo.prospectig.entities.ErrorData;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;

import java.io.IOException;

import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by ronaldruiz on 5/15/18.
 */

public abstract class BaseModel<R, V> {
    private static final String NETWORK_CONNECTION_MESSAGE = "Hubo un error en la conexión.";
    private static final int UN_PROCESSABLE_ENTITY_CODE = 422;
    private static final String GENERIC_ERROR_MESSAGE = "Error inesperado, intente otra vez";
    protected abstract DisposableObserver<R> getObserver();
    protected abstract Observable<R> getObservable(V parameters);

    public ErrorData validateError(Throwable e) {
        ErrorData errorData = null;
        if (e instanceof java.net.SocketException) {
            errorData = new ErrorData();
            errorData.setCode(e.hashCode());
            errorData.setError(NETWORK_CONNECTION_MESSAGE);
        } else if (e instanceof HttpException) {
            HttpException httpException = (HttpException) e;
            if (httpException.code() == UN_PROCESSABLE_ENTITY_CODE) {
                try {
                    Response response = httpException.response();
                    if (response != null) {
                        if (response.errorBody() != null) {
                            errorData = parseError(response.errorBody());
                        }
                    }
                } catch (NullPointerException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return errorData;
    }

    private ErrorData parseError(ResponseBody responseBody) {
        if (responseBody == null) {
            return null;
        }
        ErrorData errorData = null;
        try {
            errorData = new Gson().fromJson(responseBody.string(), ErrorData.class);
        } catch (IOException e) {
            Log.d(BaseModel.class.getName(), e.getMessage(), e);
        } finally {
            if (errorData == null) {
                errorData = new ErrorData();
                errorData.setCode(UN_PROCESSABLE_ENTITY_CODE);
                errorData.setError(GENERIC_ERROR_MESSAGE);
            }
        }
        return errorData;
    }
}
