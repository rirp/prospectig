package com.intergrupo.prospectig.ui.login.model;

/**
 * Created by ronaldruiz on 6/26/18.
 */

public interface LoginModelCallback {
    void onLoginSuccess(boolean isLocalAuthentication);
    void onLoginFailure(int pCode, String pMessage);
}
